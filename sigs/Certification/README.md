# Certification

Certification SIG 致力于对社区openGauss发行版本进行发行版本能力、成熟度的评估和认证，对openGauss社区版本的服务提供商进行服务能力的评估和认证，以及基于openGauss社区的ISV兼容性认证和人才认证工作，通过规范和引导openGauss社区版本的各项标准，以期达到支撑openGauss生态产业的繁荣发展。

主要工作包括：

- 认证标准制定，基于发行版认证、人才认证，版本成熟度，ISV兼容性认证、CSP认证等制定评估的标准。
- 认证流程制定，定义具体认证流程、评估机制、证书格式等。
- 认证内容，详细规定各个认证所需要满足的要求和评价方式。
- 认证执行，根据定义的标准和流程，通过sig会议组织各个认证的结果评议。

# 组织会议

- 公开的会议时间：北京时间 每双周一 16:00-17:00

# 成员

### Maintainer列表

- 夏团利[@touny](https://gitee.com/touny), *xiatuanli@huawei.com*
- 赵立超[@zhaolichao1234](https://gitee.com/zhaolichao1234), *zhaolichao@huawei.com*

### Committer列表

- 黄凯耀[@huangkaiyao](https://gitee.com/huangkaiyao), *kaiyao.huang@qq.com*
- 李岩松[@yansong_lee](https://gitee.com/yansong_lee), *yansong_lee@163.com*
- 张长军[@dbowner_zhang](https://gitee.com/dbowner_zhang), *zhangchangjun2@huawei.com*
- 鲁攀峰[@Alvin00372627](https://gitee.com/Alvin00372627), *lupanfeng@huawei.com*
- 徐徐[@sandy-xuxu](https://gitee.com/sandy-xuxu), *sandy.xuxu@huawei.com*
- 陈振煜[@eric14chan](https://gitee.com/eric14chan), *chenzhenyu13@huawei.com*
- 王景全[@wang-jingquan602623](https://gitee.com/wang-jingquan602623), *wangjingquan@huawei.com*
- 储其方[@chuqifang](https://gitee.com/chuqifang), *chuqifang@huawei.com*
- 吴文良[@wu-wenliang](https://gitee.com/wu-wenliang), *wuwenliangwwl.wu@huawei.com*
- 许小钦[@xuxiaoqin19](https://gitee.com/xuxiaoqin19), *xuxiaoqin2@huawei.com*
- 张歆芳[@mickymom](https://gitee.com/mickymom), *zhangxinfang@huawei.com*

# 联系方式

- *certification@opengauss.org*

# 仓库清单

仓库地址：

- https://gitee.com/opengauss/distribution-certification-testcase
- https://gitee.com/opengauss/distribution-certification
- https://gitee.com/opengauss/service-partner-certification
- https://gitee.com/opengauss/compatible-certification