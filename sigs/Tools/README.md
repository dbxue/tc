
# Tools

openGauss工具生态的长期规划，发展和演进。


# 组织会议

- 公开的会议时间：北京时间 每双周二 10:00-11:00


# 成员


### Maintainer列表

- 熊小军[@xiong_xjun](https://gitee.com/xiong_xjun)，*xiong_xiaojun@yeah.net*


### Committer列表

- 周斌[@justbk](https://gitee.com/justbk)，*249396768@qq.com*
- 彭炯[@totaj](https://gitee.com/totaj) *pengjiong1@huawei.com*
- 贺承汉[@he-chenghan](https://gitee.com/he-chenghan) *hechenghan@huawei.com*
- 张耀中[@buter](https://gitee.com/buter) *zhangyaozhong1@huawei.com*
- 窦欣[@ywzq1161327784](https://gitee.com/ywzq1161327784) *douxin5@huawei.com*
- 李臻峰[@lizhenfeng123](https://gitee.com/lizhenfeng123) *lizhenfeng001@chinasofti.com*

# 联系方式

- [邮件列表](https://mailweb.opengauss.org/postorius/lists/tools.opengauss.org/)

# 仓库清单

仓库地址：
- https://gitee.com/opengauss/openGauss-server
- https://gitee.com/opengauss/openGauss-third_party
- https://gitee.com/opengauss/openGauss-housekeeper
- https://gitee.com/opengauss/openGauss-tools-loader
- https://gitee.com/opengauss/openGauss-tools-chameleon
- https://gitee.com/opengauss/openGauss-tools-datachecker
- https://gitee.com/opengauss/openGauss-tools-migrationAssessmentReport
- https://gitee.com/opengauss/openGauss-tools-ora2og
- https://gitee.com/opengauss/openGauss-tools-onlineMigration
- https://gitee.com/opengauss/ham4db
