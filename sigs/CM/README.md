# CM SIG组

CM，集群管理系统，为opengauss数据库提供了主备的状态监控、网络通信故障监控、文件系统故障监控、故障自动主备切换等能力。同时提供了丰富的集群管理能力，如集群、节点、实例级的启停，集群状态查询、主备切换、日志管理等。相信Cluster Manage SIG的成立，可以使集群的可靠性更上一层楼。

# 组织会议

- 公开的视频会议时间：北京时间双周二下午，14：10~16:00

# 成员

### Maintainer列表

- 张建勋[@zjxqxf](https://gitee.com/zjxqxf), *zhangjianxun.zhangjianxun@huawei.com*
- 王炜[@wangwei5](https://gitee.com/wangwei5), *wangwei5@huawei.com*


### Committer列表

  - 邓勇[@dengyong2](https://gitee.com/dengyong2), *dean.dengyong@huawei.com*
  - 胡习林[@hu-xilin](https://gitee.com/hu-xilin), *huxilin@huawei.com*
  - 杨迪[@yangdi07](https://gitee.com/yangdi07), *yangdi14@huawei.com*
  - 于正[@yz_db](https://gitee.com/yz_db), *yuzheng3@huawei.com*

# 联系方式

- [邮件列表](https://mailweb.opengauss.org/postorius/lists/cm.opengauss.org/)

# 仓库清单

仓库地址：

- https://gitee.com/opengauss/CM
- https://gitee.com/opengauss/DCC
- https://gitee.com/opengauss/CBB
