
# QA

- 执行版本测试任务：开发并执行测试计划以及测试用例，来系统的测试重要功能，通常与多个测试人员合作
- 看护关键软件包质量
- 开发测试工具：开发并运行自动查询潜在缺陷的攻击
- 维护发布标准：和开发人员以及发布工程师一起维护发布标准。发布标准用来决定哪些缺陷应该在只做openGauss的预发布版或者最终发布版之前修复。
- 数据库测试的业界洞察和研究


# 组织会议

- 公开的会议时间：北京时间 每双周五15:00-17:00


# 成员


### Maintainer列表

- 张长军[@dbowner_zhang](https://gitee.com/dbowner_zhang)，*zhangchangjun2@huawei.com*
- 李岩松[@yansong_lee](https://gitee.com/yansong_lee)，*yansong_lee@163.com*

### Committer列表

- 附雄道[@xiongdaofu](https://gitee.com/xiongdaofu)，*fuxiongdao@huawei.com*
- 江文龙[@j00313273](https://gitee.com/j00313273)，*52443591@qq.com*
- 周方圆[@zhoufangyuan](https://gitee.com/zhoufangyuan)，*zhoufangyuan7@huawei.com*
- 陈栋[@chendong76](https://gitee.com/chendong76)，*1209756284@qq.com*
- 张志靖[@zhangzhijing](https://gitee.com/zhangzhijing)，*2235036288@qq.com*
- 张翱[@zhangao_za](https://gitee.com/zhangao_za)，*zhangao23@huawei.com*
- 廖诗婷[@liao-shiting](https://gitee.com/liao-shiting)，*liaost@vastdata.com.cn*
- 梁燕[@roxanne315](https://gitee.com/roxanne315)，*liangyan@vastdata.com.cn*
- 孙敏[@min-sun](https://gitee.com/min-sun)，*min.sun@enmotech.com*
- 孙静元[@sun-jingyuan](https://gitee.com/sun-jingyuan)，*sunjingyuan@shentongdata.com*
- 宋菲[@szoscar55](https://gitee.com/szoscar55)，*songfei@shentongdata.com*

# 联系方式

- [邮件列表](https://mailweb.opengauss.org/postorius/lists/qa.opengauss.org/)

# 仓库清单

仓库地址：
- https://gitee.com/opengauss/QA
